# CI/CD for Treuze projects

<br>

## Jobs

<br>

### Terraform

<br>

#### tf-plan

`/jobs/terraform/general/plan.yaml`

`terraform init + validate + plan`

Defines a job named `tf-plan`, in a stage called `tf-plan`.

Outputs a plan file as an artifact to be used by `tf-apply`.

<br>

##### Requirements

* The following environment variables: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_REGION`, `TF_VERSION` and `TF_IN_AUTOMATION`

<br>

#### tf-apply

`/jobs/terraform/general/apply.yaml`

`terraform init + apply`

Defines a job named `tf-apply`, in a stage called `tf-apply`.

<br>

##### Requirements

* The following environment variables: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_REGION`, `TF_VERSION` and `TF_IN_AUTOMATION`

<br>

#### tf-destroy

`/jobs/terraform/general/destroy.yaml`

`terraform init + destroy`

Defines a job named `tf-destroy`, in a stage called `tf-destroy`.

<br>

##### Requirements

* The following environment variables: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_REGION`, `TF_VERSION` and `TF_IN_AUTOMATION`

<br>

### awscliv2

<br>

#### s3-plan

`/jobs/awscli/js/plan.yaml`

Defines a job named `s3-plan`, in a stage called `s3-plan`.

Shows the contents of the CDN bucket, as well as the files that will be uploaded.

<br>

##### Requirements

* A bucket in S3
* The following environment variables: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_REGION` and `CDN_BUCKET_NAME`

<br>

#### s3-upload

`/jobs/awscli/js/upload.yaml`

Defines a job named `s3-upload`, in a stage called `s3-upload`.

Uploads two files to the CDN bucket - `justbytes.js` and `justbytes-$CI_COMMIT_TAG.js`. The `justbytes.js` file is overriden.

Triggered manually after `s3-plan`.

<br>

##### Requirements

* A bucket in S3
* The following environment variables: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_REGION` and `CDN_BUCKET_NAME`

<br>

### NPM

<br>

#### npm-check

`/jobs/npm/nodejs/check.yaml`

Defines a job called `npm-check`, in a stage called `check`.

Creates a `.npmrc` file with the necessary credentials to push a package into treuzedev@npm, then performs a `npm pack --dry-run` to show which files will be uploaded.

It also runs a mock install to check for errors.

This job is only triggered when a commit tag is created.

<br>

##### Requirements

* A folder named `package`, as all the commands run there and not in the repository root
* The following environment variables: `NPM_USERNAME`, `NPM_TOKEN`, `NPM_EMAIL`, `NODEJS_VERSION` and `NODEJS_ALPINE_VERSION`

<br>

#### npm-publish

`/jobs/npm/nodejs/publish.yaml`

Defines a job called `npm-publish`, in a stage called `publish`.

Creates a `.npmrc` file with the necessary credentials to push a package into treuzedev@npm, the publishes the package to treuzedev@npm.

This job is only triggered when a commit tag is created, and needs to be started manually from the UI.

<br>

##### Requirements

* A folder named `package`, as all the commands run there and not in the repository root
* The following environment variables: `NPM_USERNAME`, `NPM_TOKEN`, `NPM_EMAIL`, `NODEJS_VERSION` and `NODEJS_ALPINE_VERSION`

<br>

## Pipelines

<br>

### aws-infra

`/pipelines/aws-infra.yaml`

Triggered when a new tag is created - should indicate a new version to be released.

Runs three jobs, in a linear fashion, an automatic plan and two manually, an apply and a destroy:

* `tf-plan`
* `tf-apply`
* `tf-destroy`

This pipeline is also triggered if a commit is made to the `cicd` branch.

See each job for the pipeline requirements.

<br>

### js

`/pipelines/js.yaml`

Triggered when a new tag is created - should indicate a new version to be released.

Runs two jobs, in a linear fashion, one that shows the files that exist and the files that will be uploaded to the S3 CDN bucket, and one that actually uploads the files to the bucket:

* `s3-plan`
* `s3-upload`

This pipeline is also triggered if a commit is made to the `cicd` branch.

See each job for the pipeline requirements.

<br>

### nodejs

`/pipelines/nodejs.yaml`

Triggered when a new tag is created - should indicate a new version to be released.

Runs two jobs, in a linear fashion, one that checks if the package is ready for pushing and another that actually pushes the package to treuzedev@npm (the package publishing is manual):

* `npm-check`
* `npm-publish`

There is no destroy job as it is not possible to republish a package with the same name / version.

`npm-publish` fails if here is a version with the same tag as the one created to trigger this pipeline.

This pipeline is also triggered if a commit is made to the `cicd` branch.

See each job for the pipeline requirements.
